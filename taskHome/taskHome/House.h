#pragma once
#include "Person.h"
#include "Flat.h"


class House
{
	uint16_t number;
	uint16_t quantity;
	Flat* flat;

public:

	House(const uint16_t numberP, const uint16_t quantityP) : number{ numberP }, 
		quantity{quantityP},
		flat{ new Flat[quantityP] } {}

	House(): House{1, 1} {}

	House(const House& house) : number{house.number}, 
		quantity{ house.quantity }, flat{ new Flat[house.quantity] }
	{
		for (int i{ 0 }; i < quantity; i++)
		{
			flat[i] = house.flat[i];
		}
	}

	 House(House&& house) : number{house.number}, 
		quantity{ house.quantity },
		flat{ house.flat }
	{
		house.number = 0;
		house.quantity = 0;
		house.flat = nullptr;
	}

	House& operator=(const House& house)
	{
		if (this == &house)
		{
			return *this;
		}

		delete[] flat;

		number = house.number;
		quantity = house.quantity;
		flat = new Flat[house.quantity];

		for (int i{ 0 }; i < quantity; i++)
		{
			flat[i] = house.flat[i];
		}

		return *this;
	}

	friend istream& operator>>(istream& in, House& house)
	{
		cout << endl;
		cout << "Enter a number of house :";
		in >> house.number;
		cout << "Enter an quantity of flats in the house:";
		in >> house.quantity;
		house.flat = new Flat[house.quantity];
		for (int i{ 0 }; i < house.quantity; ++i)
		{
			in >> house.flat[i];
		}
		return in;
	}

	friend ostream& operator<<(ostream& out, const House& house)
	{
		cout << endl;
		cout << "number of house: ";
		out << house.number;
		cout << endl;
		cout << "quantity of flats: ";
		out << house.quantity;
		cout << endl;
		for (int i{ 0 }; i < house.quantity; i++)
		{
			out << house.flat[i];
		}
		cout << endl;
		return out;
	}

	~House()
	{
		delete[] flat;
	}
};

