#pragma once
#include "Person.h"
class Flat
{
	int number;
	int quantity;
	Person* person;
public :

	Flat (const int numberP, const int quantityP) : number{numberP},
		quantity{ quantityP },
		person{ new Person[numberP]}{}

	Flat() : Flat{0, 0}{}

	Flat(const Flat& flat) : number { flat.number },
		quantity{ flat.quantity },
		person{new Person [flat.quantity]}
	{
		for (int i{ 0 }; i < quantity; i++)
		{
			person[i] = flat.person[i];
		}
	}

	Flat(Flat&& flat) : number{ flat.number }, quantity{ flat.quantity },
		person{ flat.person }
	{
		flat.number = 0;
		flat.quantity = 0;
		flat.person = nullptr;
	}

	Flat& operator=(const Flat& flat)
	{
		if (this == &flat)
		{
			return *this;
		}

		delete[] person;

		number = flat.number;
		quantity = flat.quantity;
		person = new Person[flat.quantity];

		for (int i{ 0 }; i < quantity; i++)
		{
			person[i] = flat.person[i];
		}

		return *this;

	}

	friend istream& operator>>(istream& in, Flat& flat)
	{
		cout << endl;
		cout << "Enter a number of flat :";
		in >> flat.number;
		cout << "Enter an quantity of persons in the flat:";
		in >> flat.quantity;
		flat.person = new Person[flat.quantity];
		for (int i{ 0 }; i < flat.quantity; ++i)
		{
			in >> flat.person[i];
		}
		return in;
	}

	
	friend ostream& operator<<(ostream& out, const Flat& flat)
	{
		cout << endl;
		cout << "number of flat: ";
		out << flat.number;
		cout << endl;
		cout << "quantity of persons: ";
		out << flat.quantity;
		cout << endl;
		for (int i{ 0 }; i < flat.quantity; i++)
		{
			out << flat.person[i];
		}
		cout << endl;
		return out;
	}

	Person* setPerson()
	{
		person = new Person[quantity];
		for (int i{ 0 }; i < quantity; i++)
		{
		  cin >> person[i];
		}
		return person;
	}
	
	~Flat()
	{
		delete[] person;
	}
};

