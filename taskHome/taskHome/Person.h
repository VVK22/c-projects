#pragma once
#include <iostream>

using namespace std;

class Person
{
	char* name;
	char* phone;
	uint16_t age;
	
public:
	 
	Person(const char* nameP, const char* phoneP, const uint16_t ageP) : name{ nameP ? new char[strlen(nameP) + 1] : nullptr },
		phone{ phoneP ? new char[strlen(phoneP) + 1] : nullptr },
		age{ageP}
	{
		int len = strlen(nameP) + 1;
		strcpy_s(name, len, nameP);

		int lenPh = strlen(phoneP) + 1;
		strcpy_s(phone, lenPh, phoneP);
	}

	Person() : Person{ new char[500], new char[20], 1} {}

	Person(const Person& person) : name{ new char [strlen(person.name) + 1]}, 
		phone{new char [strlen(person.phone) + 1]}, age{person.age}
	{
		int lenN = strlen(person.name) + 1;
		strcpy_s(name, lenN, person.name);
		int lenPh = strlen(person.phone) + 1;
		strcpy_s(phone, lenPh, person.phone);
	}

	Person(Person&& person) : name{person.name}, 
		phone{person.phone},
		age{person.age}
	{
		person.name = nullptr;
		person.phone = nullptr;
		person.age = 0;
	}

	Person& operator=(const Person& p)
	{
		if (this == &p)
		{
			return *this;
		}

		delete[] name;
		delete[] phone;
	    name = new char[strlen(p.name) + 1];
		phone = new char[strlen(p.phone) + 1];
		age = p.age;

		strcpy_s(name, strlen(p.name) + 1, p.name);
		strcpy_s(phone, strlen(p.phone) + 1, p.phone);
		
		return *this;

	}

	friend ostream& operator<<(ostream& out, const Person& p)
	{
		cout << endl;
		cout << "name: ";
		out << p.name;
		cout << endl;
		cout << "phone: ";
		out	<< p.phone;
		cout << endl;
		cout << "age: ";
		out << p.age;
		return out;
	}

	friend istream& operator>>(istream& in, Person& p)
	{
		cout << endl;
		cout << "Enter name: ";
		in >> p.name;
		cout << "Enter phone: ";
		in >> p.phone;
		cout << "Enter age: ";
		in >> p.age;
		return in;
	}
	
	~Person()
	{
		if (name)
		{
			delete[] name;
		}

		if (phone)
		{
			delete[] phone;
		}
	}
};

