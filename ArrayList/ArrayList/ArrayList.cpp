// ArrayList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Array.h"

int main()
{
    List list;
    List list2;
    List list3;
    list.addHead(25);
    list.addHead(40);
    list.addHead(22);
    list2.addHead(56);
    list2.addHead(78);
    list2.addHead(-23);
    list3 = list + list2;

    list3.show();
    list3.setAt(22, 2);
    cout << endl;
    cout << list3[1] << endl;
    list3.show();
  
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
