#pragma once
#include <iostream>

using namespace std;


struct Node
{
	int data;
	Node* next;
	Node* prev;
};

class List
{
	Node* head;
	Node* tail;
	int count;

public:

	List() : head{ nullptr }, tail{ nullptr }, count{0} {}

	List(const List& list)
	{
		head = nullptr;
		tail = nullptr;
		count = 0;

		Node* temp = list.head;

		while (temp != 0)
		{
			addTail(temp->data);
			temp = temp->next;
		}
	}

	

	List& operator=(const List& list)
	{
		if (this == &list)
		{
			return *this;
		}

		this->~List();

		Node* temp = list.head;

		while (temp != 0)
		{
			addTail(temp->data);
			temp = temp->next;
		}

		return *this;
	}

	friend const List operator+(const List& list1, const List& list2)
	{
		List list3;

		if (list1.getSize() == list2.getSize())
		{
			int i{ 1 };
			while (i != list1.getSize() + 1)
			{
				list3.addHead(list1.getAt(i) + list2.getAt(i));
				i++;
			}
		}
		else
		{
			cout << "Different sizes";
			return list3;
		}
		
		return list3;
		
	}


	int getSize() const
	{
		return count;
	}

	bool isEampty()
	{
		return count = 0;
	}

	Node* getData(int idx) const
	{
		Node* temp = head;

		if (idx < 1 or idx > count)
		{
			cout << "Erorr! Incorrect index!";
			return 0;
		}

		int i{ 1 };

		while (i < idx and temp != 0)
		{
			temp = temp->next;
			i++;
		}

		if (temp == 0)
			return 0;
		else
			return temp;
	}

	int getAt(int idx) const
	{
		if (idx < 1 or idx > count)
		{
			cout << "Erorr! Incorrect index!";
			return 0;
		}

		Node* temp = getData(idx);
		return temp->data;
	}
	
	void setAt(int el, int idx)
	{
		if (idx < 1 or idx > count)
		{
			cout << "Erorr! Incorrect index!";
			return;
		}
		
		Node* temp = getData(idx);
		temp->data = el;
	}

	void addHead(int el)
	{
		Node* temp = new Node;

		temp->prev = 0;
		temp->data = el;
		temp->next = head;

		if (head != 0)
		{
			head->prev = temp;
		}

		if (count == 0)
			head = tail = temp;
		else
			head = temp;

		count++;
		
	}

	void addTail(int el)
	{
		Node* temp = new Node;

		temp->next = 0;
		temp->data = el;
		temp->prev = tail;

		if (tail != 0)
		{
			tail->next = temp;
		}

		if (count == 0)
			head = tail = temp;
		else
			tail = temp;

		count++;

	}
	void insertAt(int idx)
	{
		if (idx < 1 or idx > count + 1)
		{
			cout << "Erorr! Incorrect index!";
			return;
		}

		int el;
		if (idx == count + 1)
		{
			cout << "Enter new number: ";
			cin >> el;
			addTail(el);
			return;
		}
		else if (idx == 1)
		{
			cout << "Enter new number: ";
			cin >> el;
			addHead(el);
			return;
		}

		int i{ 1 };
		
		Node* ins = head;

		while (i < idx)
		{
			ins = ins->next;
			i++;
		}

		Node* prevIns = ins->prev;
		Node* temp = new Node;

		cout << "Enter new number: ";
		cin >> temp->data;

		if (prevIns != 0 and count != 1)
		{
			prevIns->next = temp;
		}

		temp->next = ins;
		temp->prev = prevIns;
		ins->prev = temp;

		count++;

	}

	void removeAt(int idx)
	{
		if (idx < 1 or idx > count)
		{
			cout << "Erorr! Incorrect index!";
			return;
		}

		int i{ 1 };

		Node* del = head;
		while (i < idx)
		{
			del = del->next;
			i++;
		}

		Node* prevDel = del->prev;
		Node* afterDel = del->next;

		if (prevDel != 0 and count != 1)
		{
			prevDel->next = afterDel;
		}

		if (afterDel != 0 and count != 1)
		{
			afterDel->prev = prevDel;
		}

		if (idx == 1)
		{
			head = afterDel;
		}

		if (idx == count)
		{
			tail = prevDel;
		}

		delete del;

		count--;
	}

	void removeAll()
	{
		while (count != 0)
		{
			removeAt(1);
		}
	}

	void show()
	{
		if (count != 0)
		{
			Node* temp = head;
			
			while (temp->next != 0)
			{
				cout << temp->data << " ";
				temp = temp->next;
			}

			cout << temp->data << " ";
		}
	}

	const int& operator[](int idx) const
	{
		if (idx < 1 or idx > count)
		{
			cout << "Erorr! Incorrect index!";
			return 0;
		}

		Node* temp = getData(idx);
		return temp->data;
	}

	~List()
	{
		removeAll();
	}
};

