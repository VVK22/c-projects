// taskTemplateArray.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#define FOR(a, b, i) for(int i {a}; i < b; i++)

using namespace std;

template <class T>
void show(vector <T> &arr)
{
    cout << endl;
    FOR(0, arr.size(), i)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template <class T>
T getMax(vector<T> &arr)
{
    T max = arr[0];
    FOR(0, arr.size(), i)
    {
        if (max < arr[i])
        {
            max = arr[i];
        }
    }
    return max;
}

template <class T>
T getMin(vector<T>& arr)
{
    T min = arr[0];
    FOR(0, arr.size(), i)
    {
        if (min > arr[i])
        {
            min = arr[i];
        }
    }
    return min;
}

template <class T>
vector <T> sort(vector <T>& arr)
{
    FOR(0, arr.size(), i)
    {
        FOR(0, arr.size() - 1, j)
        {
            if (arr[j] > arr[j + 1])
            {
                swap(arr[j], arr[j + 1]);
            }
        }
    }

    return arr;
}

template <class T>
vector<T> changeElement(vector<T>& arr, int idx, T value)
{
    FOR(0, arr.size(), i)
    {
        if (i == idx)
        {
            arr[i] = value;
        }
    }

    return arr;
}

template <class T>
T searchBinary(vector <T> arr, T key)
{
    T mid{ 0 };
    int left{ 0 };
    int right = arr.size();

    while (1)
    {
        mid = (left + right) / 2;
        if (key < arr[mid])
        {
            right = mid - 1;
        }
        else if (key > arr[mid])
        {
            left = mid + 1;
        }
        else
        {
            return mid;
        }

        if (left > right)
        {
            return -1;
        }
    }
}



int main()
{
    vector <int> arrInt = { 23, 145, 4, 7, 54, 9, 4, 22 };
    vector <double> arrDouble = { 2.4, 54.34, -4.5, 0.94, -22.44, 8.3, -9.34 };

    int act{ 0 };

    while(act != 6)
    {
        cout << endl<<endl;
        cout << "Enter an action : \n"
            << "1 - find a maximum\n"
            << "2 - find a minimum\n"
            << "3 - sort the array\n"
            << "4 - change an element of the array\n"
            << "5 - binary search\n"
            << "6 - exite\n";
       
        cin >> act;
        switch (act)
        {
        case 1:
        {
            cout << "select the data type: 1 - integer, 2 - double\n";
            cin >> act;
            if (act == 1)
            {
                show(arrInt);
                cout << "max = " << getMax(arrInt);
            }
            if (act == 2)
            {
                show(arrDouble);
                cout << "max = " << getMax(arrDouble);
            }
            break;
        }
        case 2:
        {
            cout << "select the data type: 1 - integer, 2 - double\n";
            cin >> act;
            if (act == 1)
            {
                cout << endl;
                show(arrInt);
                cout << "min = " << getMin(arrInt);
            }
            if (act == 2)
            {
                cout << endl;
                show(arrDouble);
                cout << "min = " << getMin(arrDouble);
            }
            break;
        }
        case 3:
        {
            cout << "select the data type: 1 - integer, 2 - double\n";
            cin >> act;
            if (act == 1)
            {
                cout << endl;
                show(arrInt);
                arrInt = sort(arrInt);
                show(arrInt);
            }
            if (act == 2)
            {
                cout << endl;
                show(arrDouble);
                arrDouble = sort(arrDouble);
                show(arrDouble);
            }
            break;
        }
        case 4:
        {
            int idx;
            cout << "select the data type: 1 - integer, 2 - double\n";
            cin >> act;
            
            
            if (act == 1)
            {
                int value;
                cout << endl;
                show(arrInt);
                cout << "Enter an index: ";
                cin >> idx;
                    while (idx < 0 || idx > arrInt.size() - 1)
                    {
                        cout << "Error, you went out of the array. Enter a different index: ";
                        cin >> idx;
                    }
                cout << "Enter a new value: ";
                cin >> value;
                arrInt = changeElement(arrInt, idx, value);
                show(arrInt);
            }
            if (act == 2)
            {
                double value;
                cout << endl;
                show(arrDouble);
                cout << "Enter an index: ";
                cin >> idx;
                while (idx < 0 || idx > arrDouble.size() - 1)
                {
                    cout << "Error, you went out of the array. Enter a different index: ";
                    cin >> idx;
                }
                cout << "Enter a new value: ";
                cin >> value;
                arrDouble = changeElement(arrDouble, idx, value);
                show(arrDouble);
            }
            break;
        }
        case 5:
        {
            int idx;
            cout << "select the data type: 1 - integer, 2 - double\n";
            cin >> act;

            if (act == 1)
            {
                arrInt = sort(arrInt);
                show(arrInt);
                int key;
                cout << "Enter an integer: ";
                cin >> key;

                idx = searchBinary(arrInt, key);

                if (idx >= 0)
                {
                    cout << "The integer is under the index: " << idx;
                }
                else
                {
                    cout << "There is no the integer";
                }
            }

            if (act == 2)
            {
                arrDouble = sort(arrDouble);
                show(arrDouble);
                double key;
                cout << "Enter an number: ";
                cin >> key;

                idx = searchBinary(arrDouble, key);

                if (idx >= 0)
                {
                    cout << "The number is under the index: " << idx;
                }
                else
                {
                    cout << "There is no the number";
                }
            }

            break;
        }
        

        }
    }
    


    

    

    

    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
