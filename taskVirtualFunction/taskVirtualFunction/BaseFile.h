#pragma once
#include <iostream>
#include <fstream>
#include <string>


using namespace std;
class BaseFile
{
protected:
	char* str;
	char* path;
public:

	BaseFile(const char* strP, const char* pathP) : str{strP ? new char[strlen(strP)+1] : nullptr},
		path{pathP ? new char[strlen(pathP) + 1] : nullptr } 
	{
		 strcpy_s(str, strlen(strP) + 1, strP);
		 strcpy_s(path, strlen(pathP) + 1, pathP);

	}
	explicit BaseFile(const char* pathP) : path{ pathP ? new char[strlen(pathP) + 1] : nullptr } 
	{
		strcpy_s(path, strlen(pathP) + 1, pathP);
	}
	BaseFile() : BaseFile{new char[500], new char[500]} {}

	void setStr(const char* strP)
	{
		int len = strlen(strP) + 1;
		strcpy_s(str, len, strP);
	}

	void writeToFile()
	{
		ofstream file;
		file.open(path, ios::out | ios::app);

		if (file.is_open())
		{
			file << str ;
			file.close();
			cout << "Ok" << endl;
		}
		else
			cout << "Erorr!";
	}

	virtual void display()
	{
		string readstr;
	    ifstream file;
		file.open(path);
		if (file.is_open())
		{
			while (getline(file, readstr))
			{
			  cout << readstr;
			}
			file.close();
		}
		else
			cout << "Erorr!";
	}

	~BaseFile()
	{
		delete[] str;
		delete[] path;
	}

};

class ASCII : public BaseFile
{
public:
	ASCII(const char* strP, const char* pathP)
	{
		str = new char[strlen(strP) + 1];
		strcpy_s(str, strlen(strP) + 1, strP);
		
		path = new char[strlen(pathP) + 1];
		strcpy_s(path, strlen(pathP) + 1, pathP);
	} 
	explicit ASCII(const char* pathP)
	{
		path = new char[strlen(pathP) + 1];
		strcpy_s(path, strlen(pathP) + 1, pathP);
	}
	ASCII() : ASCII{ new char[500], new char[500] } {}

	virtual void display()
	{
		ifstream file;
		file.open(path);
		if (file.is_open())
		{
			char t;
			while (file.get(t))
				if (t == '\n')
					cout << endl;
				else
					cout << static_cast<int>(t) << " ";
			file.close();
		}
		else
			cout << "Erorr!";
	}
};

class BinaryFile : public BaseFile
{
public:
	BinaryFile(const char* strP, const char* pathP)
	{
		str = new char[strlen(strP) + 1];
		strcpy_s(str, strlen(strP) + 1, strP);

		path = new char[strlen(pathP) + 1];
		strcpy_s(path, strlen(pathP) + 1, pathP);
	}
	explicit BinaryFile(const char* pathP)
	{
		path = new char[strlen(pathP) + 1];
		strcpy_s(path, strlen(pathP) + 1, pathP);
	}
	BinaryFile() : BinaryFile{ new char[500], new char[500] } {}

	virtual void display()
	{
		ifstream file;
		file.open(path);
		if (file.is_open())
		{
			char t;
			while (file.get(t))
				if (t == '\n')
					cout << endl;
				else 
				{
					for (int i = 7; i >= 0; i--)
						cout << ((static_cast<int>(t) & static_cast<int>(pow(2, i))) > 0 ? "1" : "0");
					cout << " ";
				}
			file.close();
		}
		else
			cout << "Erorr!";
	}
};
