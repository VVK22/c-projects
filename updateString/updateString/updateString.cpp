// updateString.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "String.h"

int main()
{
   BitString bit{"0y1"};
   BitString bit2{"011"};
   BitString bit3{"00011101"};
   BitString bit4;
   
    cout << endl;
    bit2.print();
    cout << endl;
    bit3.print();
  
   if (bit3 != bit2)
   {
      cout << "ok";
      cout << endl;
   }

   bit4 = bit3 + bit2;
   bit4.print();
    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
