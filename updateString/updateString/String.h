#pragma once
#include <iostream>
using namespace std;

class String
{
protected:
	char* str;
	
public:

	String(const char* strP) : str{ strP ? new char[strlen(strP) + 1] : nullptr }
	{
		int len = strlen(strP) + 1;
		for (int i = 0; i < len; i++)
		{
			str[i] = strP[i];
		}
		
	}

	String() : String{ new char[500 + 1] } {}

	String(const String& string) : str{ new char[strlen(string.str) + 1] }
	{
		strcpy_s(str, strlen(string.str) + 1, string.str);
	}

	String(String&& string) : str{ string.str }
	{
		string.str = nullptr;
	}

	String& operator=(const String& string)
	{
		if (this == &string)
		{
			return *this;
		}
		delete[] str;
		str = new char[strlen(string.str) + 1];
		strcpy_s(str, strlen(string.str) + 1, string.str);
		

		return *this;
	}

	int length() const
	{
		return strlen(str);
	}

	void enterStr()
	{
		char* strP = new char[500];
		cout << "Enter sring : ";
		cin.getline(strP, 500);
		int len = strlen(strP) + 1;
		strcpy_s(str, len, strP);

	}

	void print() const
	{
		cout << str << endl;
	}

	void clear() 
	{
		str = nullptr;
	}

   friend const String operator+(const String& str1, const String& str2) 
	{
		String str3;

		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		for (int i{ 0 }; i < len1; i++)
		{
			str3.str[i] = str1.str[i];
		}

		for (int i{ len1 }; i < len1 + len2; i++)
		{
			str3.str[i] = str2.str[i - len1];
		}
		str3.str[len1 + len2] = '\0';
		return str3;
	}

	friend bool operator !=(const String& str1, const String& str2)
	{
		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		if (len1 != len2)
			return true;
		else
		{
			for (int i{ 0 }; i < len1; i++)
			{
				if (str1.str[i] != str2.str[i])
				{
					return true;
				}
			}
			return false;
		}
		
	}

	friend bool operator==(const String& str1, const String& str2)
	{
		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		if (len1 != len2)
			return false;
		else
		{
			for (int i{ 0 }; i < len1; i++)
			{
				if (str1.str[i] != str2.str[i])
				{
					return false;
				}
			}
			return true;
		}
	}

	~String()
	{
		delete[] str;
		
	}


};

class BitString : public String
{
public:
	BitString(const char* strP)
	{
		int len = strlen(strP) + 1;
		for (int i{0}; i < len; i++)
		{
			if (strP[i] == '0' or strP[i] == '1' or strP[i] == '\0')
			{
				str[i] = strP[i];	
			}
			else 
			{
				i = len;
				cout << "string need to contain just 0 and 1";
				str[0] = '\0';
				
			}
			
		}
		
	}

	BitString() 
	{
		str = new char[500 + 1];
		str[0] = '0';
		str[1] = '\0';
	}

	BitString(const BitString& string)  
	{
		str = new char[strlen(string.str) + 1];
		strcpy_s(str, strlen(string.str) + 1, string.str);
	}

	using String :: operator=;
	
	void changeToNegative()
	{
		int len = strlen(str) + 1;
		for (int i{ 0 }; i < len; i++)
		{
			if (str[i] == '1')
			{
				str[i] = '0';
			}
			else if(str[i] == '0')
			{
				str[i] = '1';
			}
			else
			{
				str[i] = '\0';
			}
		}
	}

	friend const BitString operator+(const BitString& str1, const BitString& str2)
	{
		BitString str3;

		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		for (int i{ 0 }; i < len1; i++)
		{
			str3.str[i] = str1.str[i];
		}

		for (int i{ len1 }; i < len1 + len2; i++)
		{
			str3.str[i] = str2.str[i - len1];
		}
		str3.str[len1 + len2] = '\0';
		return str3;
	}

	friend bool operator !=(const BitString& str1, const BitString& str2)
	{
		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		if (len1 != len2)
			return true;
		else
		{
			for (int i{ 0 }; i < len1; i++)
			{
				if (str1.str[i] != str2.str[i])
				{
					return true;
				}
			}
			return false;
		}

	}

	friend bool operator==(const BitString& str1, const BitString& str2)
	{
		int len1 = strlen(str1.str);
		int len2 = strlen(str2.str);

		if (len1 != len2)
			return false;
		else
		{
			for (int i{ 0 }; i < len1; i++)
			{
				if (str1.str[i] != str2.str[i])
				{
					return false;
				}
			}
			return true;
		}
	}


	~BitString() {};
	
};

