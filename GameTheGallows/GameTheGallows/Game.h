#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <ctime>

using namespace std;

class File
{
protected:
	map<int, string> getWords()
	{
		string word;
		map<int, string> maps;//��� ��������� ��� ������������� ����� �� �����
		
		ifstream file;
		file.open("game\\words.txt");
		if (file.is_open())
		{
			int i{ 0 };

			while (getline(file, word))
			{
				word;
				maps[i] = word;//���������� ������������� ����
				i++;
			}
			file.close();
		}
		else
		{
			cout << "Erorr!";
			return maps;
		}

		return maps;
		
    }

	string getRandomWord()
	{
		srand(time(0));
		map<int, string> maps = getWords();
		int i = rand() % maps.size();// �������� �� ����� ���������� ������������� �����
		return maps[i];
	}

	string eraseString(string str)//���������� �������������� ����� �� ���� �����
	{
		for (int i{ 0 }; i < str.size(); i++)
		{

			if (str[i] == ' ')
			{
				str.erase(0, i + 1);
				return str;
			}

		}
	}

	string toString(vector<int> vec)// ������� �������������� �����
	{
		string acii;
		for (int i = 0; i < vec.size(); i++)
		{
			acii += static_cast<char>(vec[i]);
		}
		return acii;
	}

	map<int, char> getLetters()
	{
		string beforeTran = getRandomWord();// ������ ������ ������������� �����
		vector<int> vec;//������, ������� ����� ��������� ����� �������������� �����
		int number;// ���������� �������� ����� ������������� �����
		while (beforeTran != "#")
		{
			number = stoi(beforeTran);
			vec.push_back(number);
			beforeTran = eraseString(beforeTran);//���������� �������������� ����� �� ���� �����
		}
		string afterTran = toString(vec);// ������ �������� �������������� �����
		const char* myWord = afterTran.c_str();// ��� �������� �������������� �����

		map<int, char> mult;// ��� �������� ����� ����� ���������� �� ���
		for (int i{ 0 }; i < strlen(myWord); i++)
		{
			if(myWord[i] != ' ')
			mult[i] = myWord[i];
		}

		return mult;
	}

	template<class contener>
	void showContener(const contener& cont)//������� ��������� ���������
	{
		for (auto i : cont)
			cout << i << " ";
		cout << endl;
	}


	template<class contener, class T>
	void fillContener(contener& cont, int count, T contain)// ������� ��������� ���������
	{
		for (int i{ 0 }; i < count; i++)
			cont.push_back(contain);

	}

	
	
};

class Statistics : public File
{
protected:

	int time;//����������� �����
	int attempts;// ���-�� ���� �������
	vector<char> statisticsLetters;// ������ ��������� ��� ����� ������������
	
	void showStatistics(clock_t start)
	{
		clock_t finish = clock(); //��������� ����� ���������
			//������� ��������� ��� ������� ��������� � ���������� ������� (� ��������)
		time = (finish - start) / CLOCKS_PER_SEC;
		cout << "You spended " << time << " second\n";
		cout << endl;
		cout << "Your letters: ";
		showContener(statisticsLetters);
		cout << endl;
		cout << "Number of attempts: " << attempts;
		cout << endl;
	}

	void showMyWord(map<int, char> mult)//������� ���������� ���������� �����
	{
		map<int, char>::const_iterator it;
		for (it = mult.begin(); it != mult.end(); ++it)
		{
			cout << it->second << " ";
		}
	}
	
};


class Game :  public Statistics
{
	
	void getPicture(int fail)//�������, ������ �������� ����� �������� �������
	{
		switch (fail)
		{
		case 1:
		{
			cout << endl;
			cout << "        $$$$$$$$$$$$$$$\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << " $$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			cout << " $                          $\n";
			break;
		}
		case 2:
		{
			cout << endl;
			cout << "        $$$$$$$$$$$$$$$\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        O             $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << " $$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			cout << " $                          $\n";
			break;
		}
		case 3:
		{
			cout << endl;
			cout << "        $$$$$$$$$$$$$$$\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        O             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << " $$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			cout << " $                          $\n";
			break;
		}
		case 4:
		{
			cout << endl;
			cout << "        $$$$$$$$$$$$$$$\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        O             $\n";
			cout << "      / | \\           $\n";
			cout << "        |             $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << " $$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			cout << " $                          $\n";
			break;
		}
		case 5:
		{
			cout << endl;
			cout << "        $$$$$$$$$$$$$$$\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        |             $\n";
			cout << "        O             $\n";
			cout << "      / | \\           $\n";
			cout << "        |             $\n";
			cout << "       / \\            $\n";
			cout << "                      $\n";
			cout << "                      $\n";
			cout << " $$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			cout << " $                          $\n";
			break;
		}
		}
	}

	
	

	
public:

	void play()
	{
		map<int, char> mult = getLetters();// ��� ������ ����� ����������� �����
		vector<char> vec;//������ ��������� ����� ������������, ������� ���� � ���������� �����
		
		fillContener(vec, mult.size(), '__');
		
		map<int, char>::const_iterator it;
		cout << "\t\t\tThe gallows\n\n";
		cout << "Word containts " << mult.size()<<" letters\n";
		
		showContener(vec);
	
		char letter;//�����, ������� ������ ������������
		int gallows{ 5 };//������������ ���-�� �������� �������
		int fail{ 0 };//������� ������� ���-�� �������� �������
		int win{ 0 };//������� ������� ���-�� ������ �������
		attempts = 0;//������� ������� ���-�� ���� �������
		cout << endl;
		
		cout << "Enter a letter\n";
		clock_t start = clock(); //������� ������ ������� � ��������� � ��� ����� ������ ���������
		while (fail < gallows)
		{
			cout << endl;
			cin >> letter;
			attempts++;
			statisticsLetters.push_back(letter);//������ ��������� ��� ����� ��������� �������������
			int count{ 0 };//������� ��������� ���� �� ����� ������������ � �����.
						   // ���� �� count ������������ �� ����, ��� - �������� ����� 0
			for (it = mult.begin(); it != mult.end(); ++it)
			{
				
				if (it->second == letter)
				{
					vec.at(it->first) = it->second;
					cout << endl;
					showContener(vec);
					cout << endl;
					count++;
					win++;
					
				}
			}
			if (count == 0)
			{
				fail++;
				getPicture(fail);// �������� �������� � ����������� �� ���-�� �������� �������
			}
			if (win == mult.size())//���� ������������ ������� �����
			{
				cout << endl << endl;
				cout << "\t\t\tYOU WON !\n";
				cout << endl;
				cout << "\tStatistics:\n";//����� ����������
				showStatistics(start);
				cout << endl;
				cout << "Word: ";
				showMyWord(mult);
				cout << endl;
				return;
			}
			
			
		}
		cout << endl << endl;//���� ������������ ��������
		cout << "\t\t\tGAME OVER !\n";
		cout << endl;
		cout << "\tStatistics:\n";
		showStatistics(start);//����� ����������
		cout << endl;
		cout << "Word: ";
		showMyWord(mult);
		cout << endl;

	}

	
	 
};



