#pragma once
#include <iostream>
#include <string>

using namespace std;

template <class T>
class Matrix
{
	int rows;
	int columns;
	T* array;

	int idx(int i, int j) const
	{
		return i * columns + j;
	}

public:
	
Matrix(const int row, const int column) : rows{ row }, columns{ column }, array{ new T [row * column] }
{
	
	for (int i{ 0 }; i < rows; i++)
	{
		for (int j{ 0 }; j < columns; j++)
		{
			array[idx(i, j)] = rand() % 100;
		}

	}
}

	Matrix() : Matrix{10, 10} {}

	Matrix(const Matrix& matrix) = delete;

	Matrix(Matrix&& matrix) : rows{ matrix.rows }, columns{ matrix.columns },
		array{ matrix.array }
	{
		matrix.rows = 0;
		matrix.columns = 0;
		matrix.array = nullptr;
	}

	Matrix& operator=(const Matrix& matrix)
	{
		if (this == &matrix)
		{
			return *this;
		}
		delete[] array;

		array = new T[matrix.rows * matrix.columns];
		rows = matrix.rows;
		columns = matrix.columns;

		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				array[idx(i, j)] = matrix.array[idx(i, j)];
			}
		}

		return *this;
	}

	T& operator()(int i, int j)
	{
		return *(array + idx(i, j));
	}

	T operator()(int i, int j) const
	{
		return *(array + idx(i, j));
	}

    void showMatrix() const
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				cout << (*this)(i, j) << " ";
			}
			cout << endl;
		}
	}

	friend istream& operator>>(istream& in, Matrix& matrix)
	{
		cout << endl;
		cout << "Enter quantity of rows: ";
		in >> matrix.rows;
		cout << "Enter quantity of columns: ";
		in >> matrix.columns;
		cout << "Enter values of matrix\n";
		for (int i{ 0 }; i < matrix.rows; i++)
		{
			for (int j{ 0 }; j < matrix.columns; j++)
			{
				in >> matrix.array[i * matrix.columns + j];
			}
			cout << endl;
	         
		}
		return in;
	}

	friend ostream& operator<<(ostream& out, const Matrix& matrix)
	{
		for (int i{ 0 }; i < matrix.rows; i++)
		{
			for (int j{ 0 }; j < matrix.columns; j++)
			{
				out << matrix.array[i * matrix.columns + j] << " ";
			}
			cout << endl;
		}

		return out;
	}

	T getMin() const
	{
		T min = array[idx(0, 0)];
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				if (min > array[idx(i, j)])
				{
					min = array[idx(i, j)];
				}
				
			}
			
		}

		return min;
	}


	T getMax() const
	{
		T max = array[idx(0, 0)];
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				if (max < array[idx(i, j)])
				{
					max = array[idx(i, j)];
				}
					
			}

		}

		return max;
	}

	Matrix& operator++()
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				++array[idx(i, j)];
			}
		}

		return *this;
	}

	Matrix& operator--()
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				--array[idx(i, j)];
			}
		}

		return *this;
	}

	Matrix& operator++(T)
	{
		Matrix <T> matrix;
		++(*this);
		return matrix;

	}

	Matrix& operator--(T)
	{
		Matrix <T> matrix;
		--(*this);
		return matrix;
	}

	static Matrix returnEmptyArray() 
	{
		Matrix <T> result{ 0, 0 };
		cout << "The operation cannot be performed. The sizes of arrays are different";
		return result;
	}

	friend bool operator==(const Matrix& matrix1, const Matrix& matrix2)
	{
		return matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns;
	}

	friend const Matrix operator+(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <T> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						+ matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator-(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <T> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						- matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator*(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <T> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						* matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator/(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <T> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					if (matrix2.array[i * matrix2.columns + j] == 0)
					{
						matrix2.array[i * matrix2.columns + j] = 1;
					}
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						/ matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}


	~Matrix()
	{
		delete[] array;
	}
};



template <>
class Matrix <double>
{
	int rows;
	int columns;
    double* array;

	int idx(int i, int j) const
	{
		return i * columns + j;
	}

public:

	Matrix(const int row, const int column) : rows{ row }, columns{ column },
		array{ new double [row * column] }
	{

		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				array[idx(i, j)] = rand() % 100 * 0.1;
			}

		}
	}

	Matrix() : Matrix{ 20, 20 } {}

	Matrix(const Matrix& matrix) = delete;

	Matrix(Matrix&& matrix) : rows{ matrix.rows }, columns{ matrix.columns },
		array{ matrix.array }
	{
		matrix.rows = 0;
		matrix.columns = 0;
		matrix.array = nullptr;
	}

	Matrix& operator=(const Matrix& matrix)
	{
		if (this == &matrix)
		{
			return *this;
		}
		delete[] array;

		array = new double[matrix.rows * matrix.columns];
		rows = matrix.rows;
		columns = matrix.columns;

		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				array[idx(i, j)] = matrix.array[idx(i, j)];
			}
		}

		return *this;
	}

	double& operator()(int i, int j)
	{
		return *(array + idx(i, j));
	}

	double operator()(int i, int j) const
	{
		return *(array + idx(i, j));
	}

	void showMatrix() const
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				cout << (*this)(i, j) << " ";
			}
			cout << endl;
		}
	}

	friend istream& operator>>(istream& in, Matrix& matrix)
	{
		cout << endl;
		cout << "Enter quantity of rows: ";
		in >> matrix.rows;
		cout << "Enter quantity of columns: ";
		in >> matrix.columns;
		cout << "Enter values of matrix\n";
		for (int i{ 0 }; i < matrix.rows; i++)
		{
			for (int j{ 0 }; j < matrix.columns; j++)
			{
				in >> matrix.array[i * matrix.columns + j];
			}
			cout << endl;

		}
		return in;
	}

	friend ostream& operator<<(ostream& out, const Matrix& matrix)
	{
		for (int i{ 0 }; i < matrix.rows; i++)
		{
			for (int j{ 0 }; j < matrix.columns; j++)
			{
				out << matrix.array[i * matrix.columns + j] << " ";
			}
			cout << endl;
		}

		return out;
	}

	double getMin() const
	{
		double min = array[idx(0, 0)];
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				if (min > array[idx(i, j)])
				{
					min = array[idx(i, j)];
				}

			}

		}

		return min;
	}


	double getMax() const
	{
		double max = array[idx(0, 0)];
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				if (max < array[idx(i, j)])
				{
					max = array[idx(i, j)];
				}

			}

		}

		return max;
	}

	Matrix& operator++()
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				++array[idx(i, j)];
			}
		}

		return *this;
	}

	Matrix& operator--()
	{
		for (int i{ 0 }; i < rows; i++)
		{
			for (int j{ 0 }; j < columns; j++)
			{
				--array[idx(i, j)];
			}
		}

		return *this;
	}

	Matrix& operator++(int)
	{
		Matrix matrix;
		++(*this);
		return matrix;

	}

	Matrix& operator--(int)
	{
		Matrix matrix;
		--(*this);
		return matrix;
	}

	static Matrix returnEmptyArray() 
	{
		Matrix result{ 0, 0 };
		cout << "The operation cannot be performed. The sizes of arrays are different";
		return result;
	}

	friend bool operator==(const Matrix& matrix1, const Matrix& matrix2)
	{
		return matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns;
	}

	friend const Matrix operator+(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						+ matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator-(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <double> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						- matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator*(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <double> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						* matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}

	friend const Matrix operator/(const Matrix& matrix1, const Matrix& matrix2)
	{
		if (matrix1.rows == matrix2.rows and matrix1.columns == matrix2.columns)
		{
			Matrix <double> result{ matrix1.rows, matrix1.columns };
			for (int i{ 0 }; i < matrix1.rows; i++)
			{
				for (int j{ 0 }; j < matrix1.columns; j++)
				{
					if (matrix2.array[i * matrix2.columns + j] == 0)
					{
						matrix2.array[i * matrix2.columns + j] = 1;
					}
					result.array[i * result.columns + j] = matrix1.array[i * matrix1.columns + j]
						/ matrix2.array[i * matrix2.columns + j];
				}
			}

			return result;
		}
		else
		{
			returnEmptyArray();
		}
	}



	~Matrix()
	{
		delete[] array;
	}
};