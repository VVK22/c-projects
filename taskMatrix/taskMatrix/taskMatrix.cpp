// taskMatrix.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Matrix.h"

int main()
{ 
    Matrix <int> mat1;
    Matrix <int> mat2;
    int act{ 0 };

    while (act != 7)
    {
        cout << endl << endl;
        cout << "Enter an action : \n"
            << "1 - find a maximum\n"
            << "2 - find a minimum\n"
            << "3 - addition\n"
            << "4 - subtraction\n"
            << "5 - multiplication\n"
            << "6 - division\n"
            << "7 - exite\n";

        cin >> act;
        switch (act)
        {
        case 1:
        {
            Matrix <double> mat{7, 7};
            cout << endl << mat << endl;
            cout << "max = " << mat.getMax();
            break;
        }
        case 2:
        {
            Matrix <double> mat{5, 5};
            cout << endl << mat << endl;
            cout << "min = " << mat.getMin();
            break;
        }
        case 3:
        {
            cin >> mat1;
            cout << endl << mat1 << endl;
            cin >> mat2;
            cout << endl << mat2 << endl;
            if (mat1 == mat2)
            {
                cout << "answer: " << endl << mat1 + mat2;
                break;
            }
            else
            {
                cout << "The operation cannot be performed. The sizes of arrays are different";
                break;
            }
        }
        case 4:
        {
            cin >> mat1;
            cout << endl << mat1 << endl;
            cin >> mat2;
            cout << endl << mat2 << endl;
            if (mat1 == mat2)
            {
                cout << "answer: " << endl << mat1 - mat2;
                break;
            }
            else
            {
                cout << "The operation cannot be performed. The sizes of arrays are different";
                break;
            }
        }
        case 5:
        {
            cin >> mat1;
            cout << endl << mat1 << endl;
            cin >> mat2;
            cout << endl << mat2 << endl;
            if (mat1 == mat2)
            {
                cout << "answer: " << endl << mat1 * mat2;
                break;
            }
            else
            {
                cout << "The operation cannot be performed. The sizes of arrays are different";
                break;
            }
        }
        case 6:
        {
            cin >> mat1;
            cout << endl << mat1 << endl;
            cin >> mat2;
            cout << endl << mat2 << endl;
            if (mat1 == mat2)
            {
                cout << "answer: " << endl << mat1 / mat2;
                break;
            }
            else
            {
                cout << "The operation cannot be performed. The sizes of arrays are different";
                break;
            }
        }

        }
    }
	
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
