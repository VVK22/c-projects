#pragma once
#include <iostream>

using namespace std;

class Fraction
{
	int numerator;
	int denominator;

	static const int findCommonDenom(const int denom1, const int denom2)
	{
		int common = 1;
		if (denom1 % denom2 == 0)
		{
			common = denom1;
		}
		else if (denom2 % denom1 == 0)
		{
			common = denom2;
		}
		else
		{
			common = denom1 * denom2;
		}

		return common;

	}

public:

	

	Fraction(int numeratorP, int denominatorP) : numerator{ numeratorP }, denominator{ denominatorP }
	{
		if (denominator == 0)
		{
			cout << "Error! The denominator can't be 0\n";
			denominator = 1;
		}
	}

	Fraction() : Fraction{ 1, 1 } {}

	Fraction(const Fraction& frac) = delete;

	Fraction(Fraction&& frac) : numerator{ frac.numerator }, denominator{ frac.denominator }
	{
		frac.denominator = 0;
		frac.numerator = 0;
	}

	Fraction& operator=(const Fraction& frac)
	{
		if (this == &frac)
		{
			return *this;
		}
		else
		{
			numerator = frac.numerator;
			denominator = frac.denominator;
			return *this;
		}
	}

	friend ostream& operator<<(ostream& out, const Fraction& frac)
	{
		out << frac.numerator;
		cout << "\\";
		out << frac.denominator;
		return out;
	}

	friend istream& operator>>(istream& in, Fraction& frac)
	{
		cout << endl;
		cout << "Enter a numerator: ";
		in >> frac.numerator;
		cout << "Enter a denominator: ";
		in >> frac.denominator;
		if (frac.denominator == 0)
		{
			cout << "Error! The denominator can't be 0. Enter another value: ";
			in >> frac.denominator;
		}
		return in;
	}

	Fraction& operator++()
	{
		++numerator;
		++denominator;
		return* this;
	}

	Fraction& operator--()
	{
		--numerator;
		--denominator;
		return *this;
	}

	Fraction& operator++(int)
	{
		Fraction frac{numerator, denominator};
		++(*this);
		return frac;
	}

	Fraction& operator--(int)
	{
		Fraction frac{ numerator, denominator };
		--(*this);
		return frac;
	}


	friend const Fraction operator*(const Fraction& frac1, const Fraction& frac2)
	{
	   return Fraction(frac1.numerator * frac2.numerator, frac1.denominator * frac2.denominator);
	}

	friend const Fraction operator/(const Fraction& frac1, const Fraction& frac2)
	{
		return Fraction(frac1.numerator * frac2.denominator, frac2.numerator * frac1.denominator);
	}

	static Fraction add(const Fraction& frac1, const Fraction& frac2)
	{

		int common = findCommonDenom(frac1.denominator, frac2.denominator);
		int firstNum = common / frac1.denominator * frac1.numerator;
		int secondNum = common / frac2.denominator * frac2.numerator;
		return Fraction(firstNum + secondNum, common);
	}
	
	static Fraction minus(const Fraction& frac1, const Fraction& frac2)
	{

		int common = findCommonDenom(frac1.denominator, frac2.denominator);
		int firstNum = common / frac1.denominator * frac1.numerator;
		int secondNum = common / frac2.denominator * frac2.numerator;
		return Fraction(firstNum - secondNum, common);
	}

	friend const Fraction operator+(const Fraction& frac1, const Fraction& frac2)
	{
		return add(frac1, frac2);
	}

	friend const Fraction operator-(const Fraction& frac1, const Fraction& frac2)
	{
		return minus(frac1, frac2);
	}

	~Fraction(){}
};

