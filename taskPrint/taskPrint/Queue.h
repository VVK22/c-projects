#pragma once
#include <iostream>
using namespace std;
template <class T>
class Queue
{
	int maxLength;
	T* wait;
	int length;

public:
	explicit Queue(int maxLengthP) : maxLength{ maxLengthP },
		wait{ new T[maxLengthP] }, length{ 0 } {}

	
	Queue(const Queue& queue) = delete;

	void clear()
	{
		length = 0;
	}

	bool isEmpty()
	{
		return length == 0;
	}

	bool isFull()
	{
		return length == maxLength;
	}

	void add(int element)
	{
		if (!isFull())
		{
			wait[length++] = element;
		}
	}

	int extract() {
		if (!isEmpty()) {
			int el = wait[0];
			for (int i{ 0 }; i < length - 1; i++) {
				wait[i] = wait[i + 1];
			}
			length--;
			return el;
		}
		else {
			return -1;
		}
	}

	int getCount() const
	{
		return length;
	}

	void show()
	{
		for (int i{ 0 }; i < length; i++)
		{
			cout << wait[i] << " ";
		}
	}



	~Queue()
	{
		delete[] wait;
	}

};


