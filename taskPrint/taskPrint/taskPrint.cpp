// taskPrint.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Queue.h"
#include "QueuePriority.h"
//#include <queue>


int main()
{
    int count;
    cout << "Enter number of users: ";
    cin >> count;

    QueuePriority<int> queueClient{count};

    Queue <int> timeQueue{count};

    int client;
    int priority;
    int time = 1;
    int i = 0;

   
    while (i < count)
    {
        cout << "Enter the user's number: ";
        cin >> client;
        cout << "Enter the user's priopity: 1 - low, 2 - middle, 3 - high: ";
        cin >> priority;
        queueClient.add(client, priority);
        timeQueue.add(time);
        time += 3;
        i++;
    }

    cout << "\n\t\tStatistic\n\n";
    while (!queueClient.isEampty())
    {
        cout << endl;
        cout << "User number: ";
        cout << queueClient.extract() << " spend " << timeQueue.extract() <<" minuts\n";
    }

   

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
