#pragma once
#include <iostream>
using namespace std;

template <class T>
class QueuePriority
{
	T* wait;
	T* pri;
	int maxLength;
	int length;

public:
	 QueuePriority(int maxLenghtP) : wait{new T[maxLenghtP]}, 
		pri{new T[maxLenghtP]}, maxLength{maxLenghtP}, length{0} {}

	
	QueuePriority(const QueuePriority& queue) = delete;

	void top()
	{
		cout << wait[length - 1] << endl;
	}

	void clear()
	{
		length == 0;
	}

	bool isEampty()
	{
		return length == 0;
	}

	bool isFull()
	{
		return length == maxLength;
	}

	void add(int queue, int priority)
	{
		if (!isFull())
		{
			wait[length] = queue;
			pri[length] = priority;
			length++;
		}
	}

	int extract()
	{
		if (!isEampty())
		{
			int maxPri = pri[0];
			int posMaxPri = 0;

			for (int i = 1; i < length; i++)
			{
				if (maxPri < pri[i])
				{
					maxPri = pri[i];
					posMaxPri = i;
				}

			}

			int temp1 = wait[posMaxPri];
			int temp2 = pri[posMaxPri];

			for (int i = posMaxPri; i < length - 1; i++)
			{
				wait[i] = wait[i + 1];
				pri[i] = pri[i + 1];
			}

			length--;
			return temp1;
		}
		else
			return -1;
	}

	int getCount()
	{
		return length;
	}

	void show()
	{
		for (int i = 0; i < length; i++)
		{
			cout << wait[i] << " - " << pri[i] << "\n\n";
		}
	}

	~QueuePriority()
	{
		delete[] wait;
		delete[] pri;
	}
};

