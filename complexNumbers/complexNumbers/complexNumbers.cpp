// complexNumbers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Complex.h"

int main()
{
    Complex num1;
    Complex num2;

    int act{ 0 };
    while (act != 5)
    {
        cout << endl;
        cout << "Enter an action : \n"
            << "1 - add fractions\n"
            << "2 - subtract fractions\n"
            << "3 - multiply fractions\n"
            << "4 - divide fractions\n"
            << "5 - exite\n";
        cin >> act;
        switch (act)
        {
        case 1:
        {
            cin >> num1 >> num2;
            cout << "The answer: " << num1 + num2;
            break;
        }
        case 2:
        {
            cin >> num1 >> num2;
            cout << "The answer: " << num1 - num2;
            break;
        }
        case 3:
        {
            cin >> num1 >> num2;
            cout << "The answer: " << num1 * num2;
            break;
        }
        case 4:
        {
            cin >> num1 >> num2;
            cout << "The answer: " << num1 / num2;
            break;
        }
        }
        cout << endl;
    }


}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
