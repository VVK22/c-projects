#pragma once
#include <iostream>

using namespace std;

class Complex
{
	double a;
	double bi;

public:

	Complex(const double aP, const double biP) : a{ aP }, bi{biP} {}
	
	Complex() : Complex{ 1, 1 } {}

	Complex(const Complex& comp) = delete;

	Complex(Complex&& comp) : a{ comp.a }, bi{ comp.bi }
	{
		comp.a = 0;
		comp.bi = 0;
	}

	Complex& operator =(const Complex& comp)
	{
		if (this == &comp)
		{
			return *this;
		}
		else
		{
			a = comp.a;
			bi = comp.bi;

			return *this;
		}
		
	}

	friend ostream& operator<<(ostream& out, const Complex& comp)
	{
		cout << endl;
		out << comp.a;
		cout << " + ";
		out << comp.bi;
		cout << "i";
		return out;
	}

	friend istream& operator>>(istream& in, Complex& comp)
	{
		cout << endl;
		cout << "Enter the first number: ";
		in >> comp.a;
		cout << "Enter the second number: ";
		in >> comp.bi;
		return in;
	}

	Complex& operator++()
	{
		++a;
		++bi;
		return *this;
	}

	Complex& operator--()
	{
		--a;
		--bi;
		return *this;
	}

	Complex& operator++(int)
	{
		Complex comp;
		++(*this);
		return comp;
	}

	Complex& operator--(int)
	{
		Complex comp;
		--(*this);
		return comp;
	}

	friend const Complex operator+(const Complex& comp1, const Complex& comp2)
	{
		return Complex(comp1.a + comp2.a, comp1.bi + comp2.bi);
	}

	friend const Complex operator-(const Complex& comp1, const Complex& comp2)
	{
		return Complex(comp1.a - comp2.a, comp1.bi - comp2.bi);
	}

	friend const Complex operator*(const Complex& comp1, const Complex& comp2)
	{
		return Complex(comp1.a * comp2.a - comp1.bi * comp2.bi, comp1.bi * comp2.a + comp1.a * comp2.bi);
	}

	friend const Complex operator/(const Complex& comp1, const Complex& comp2)
	{
		return Complex((comp1.a * comp2.a + comp1.bi * comp2.bi) / (comp2.a * comp2.a + comp2.bi * comp2.bi), 
			(comp1.bi * comp2.a - comp1.a * comp2.bi) / (comp2.a * comp2.a + comp2.bi * comp2.bi));
	}

}; 

