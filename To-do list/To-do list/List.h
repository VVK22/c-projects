#pragma once
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using std::cout;
using std::cin;
using std::endl;

struct List
{
    string name;
    string description;
    string data;
    string executionTime;
    string priority;
};

enum class Menu
{
    OPEN_CASE = 1,
    WRITE_CASE,
    EDIT_CASE,
    FIND_CASE,
    DELETE_CASE,
    SORT_CASES, 
    
};

List* getArrList()
{

    int size = 0;
    int sizebuf = 0;

    string* ptrList = nullptr;
    string* bufList = nullptr;

    ifstream file;
    file.open("toDo\\output.txt");
    if (file.is_open())
    {
        string oneList;

        while (!file.eof())
        {
            getline(file, oneList);
            if (oneList == "#")
            {
                size++;
            }
            else if (oneList.empty())
            {
                continue;
            }
            else
            {
                int len = oneList.length();
                int count = 0;
                for (int i = 0; i < len; i++)
                {
                    if (oneList[i] == ':')
                    {
                        count += 2;
                        break;
                    }
                    count++;
                }
                oneList.erase(0, count);

                sizebuf++;
                bufList = new string[sizebuf];

                for (int i = 0; i < sizebuf - 1; i++)
                {
                    bufList[i] = ptrList[i];
                }
                bufList[sizebuf - 1] = oneList;

                delete[] ptrList;

                ptrList = bufList;
                bufList = nullptr;
            }


        }

        List* arrList = new List[size];
        int p = 0;
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < 1; j++)
            {
                arrList[i].name = ptrList[j + p];
                p++;
                arrList[i].description = ptrList[j + p];
                p++;
                arrList[i].data = ptrList[j + p];
                p++;
                arrList[i].executionTime = ptrList[j + p];
                p++;
                arrList[i].priority = ptrList[j + p];
                p++;
            }
        }


        delete[] ptrList;

        return arrList;
    }
    else
    {
        cout << "Erorr!";
    }
    file.close();
}

int getSize()
{
    int size = 0;
    ifstream file;
    file.open("toDo\\output.txt");
    if (file.is_open())
    {
        string oneList;

        while (!file.eof())
        {
            getline(file, oneList);
            if (oneList == "#")
            {
                size++;
            }
        }

        return size;
    }
    else
    {
        cout << "Erorr!";
    }
    file.close();

}

void writeList(List* arrList, int numberCase)
{
    for (int i = 0; i < numberCase; i++)
    {
        cout << "-----------------------\n";
        cout << "Enter name of case: \n";
        if(i == 0)
        cin.ignore();
        getline(cin, arrList[i].name);
        cout << endl;
        cout << "Enter description of case: \n";
        getline(cin, arrList[i].description);
        cout << endl;
        cout << "Enter data of case: \n";
        getline(cin, arrList[i].data);
        cout << endl;
        cout << "Enter execution time of case: \n";
        getline(cin, arrList[i].executionTime);
        cout << endl;
        cout << "Choose a priority: High, Medium, Low\n";
        getline(cin, arrList[i].priority);
        cout << "-----------------------\n";
        cout << endl;
    }
    ofstream out;
    out.open("toDo\\output.txt", ios::out | ios::app);


    if (out.is_open())
    {
        for (int i = 0; i < numberCase; i++)
        {
            out << "#" << endl;
            out << "Name of case: " << arrList[i].name << endl;
            out << "Description of case: " << arrList[i].description << endl;
            out << "Data of case: " << arrList[i].data << endl;
            out << "Execution time of case: " << arrList[i].executionTime << endl;
            out << "Priority: " << arrList[i].priority << endl;
            out << endl;
        }
        out.close();
    }
    else
    {
        cout << "Erorr!";
    }



}

void readList()
{
    string str;
    ifstream in;
    in.open("toDo\\output.txt");
    if (in.is_open())
    {

        while (getline(in, str))
        {
            cout << str << endl;
        }
        in.close();
    }
    else
    {
        cout << "Erorr";
    }

}

void findList()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;

    cout << "Enter name of the case : \n";
    cin.ignore();
    getline(cin, str);

    for (int i = 0; i < size; i++)
    {
        if (arrList[i].name == str)
        {
            cout << endl;
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }

    delete[] arrList;
}

void findList1()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;

    cout << "Enter priority of case : High, Medium, Low\n";
    cin.ignore();
    getline(cin, str);

    for (int i = 0; i < size; i++)
    {
        if (arrList[i].priority == str)
        {
            cout << endl;
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }

    delete[] arrList;
}

void findList2()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;

    cout << "Enter description of case\n";
    cin.ignore();
    getline(cin, str);

    for (int i = 0; i < size; i++)
    {
        if (arrList[i].description == str)
        {
            cout << endl;
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }
    delete[] arrList;
}

void findList3()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;

    cout << "Enter execution time of case\n";
    cin.ignore();
    getline(cin, str);

    for (int i = 0; i < size; i++)
    {
        if (arrList[i].executionTime == str)
        {
            cout << endl;
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }
    delete[] arrList;
}

void deleteList(string name)
{
    List* arrList = getArrList();
    int size = getSize();

    ofstream file;
    file.open("toDo\\output.txt");

    if (file.is_open())
    {
        for (int i = 0; i < size; i++)
        {
            if (arrList[i].name == name)
            {
                continue;
            }
            file << "#" << endl;
            file << "Name of case: " << arrList[i].name << endl;
            file << "Description of case: " << arrList[i].description << endl;
            file << "Data of case: " << arrList[i].data << endl;
            file << "Execution time of case: " << arrList[i].executionTime << endl;
            file << "Priority: " << arrList[i].priority << endl;
            file << endl;
        }
    }
    else
    {
        cout << "Erorr!";
    }
    file.close();
    delete[] arrList;
}

void makeChoiceDelete()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;

    cout << "Enter the name of the case you want to delete\n" << endl;
    for (int i = 0; i < size; i++)
    {
        cout << endl;
        cout << "#" << endl;
        cout << "Name of case: " << arrList[i].name << endl;
        cout << "Description of case: " << arrList[i].description << endl;
        cout << "Data of case: " << arrList[i].data << endl;
        cout << "Execution time of case: " << arrList[i].executionTime << endl;
        cout << "Priority: " << arrList[i].priority << endl;
        cout << endl;
    }

    cout << endl;
    cout << "Enter name : ";
    cin.ignore();
    getline(cin, str);
    delete[] arrList;
    deleteList(str);

}

List* editList(string name, int field, string newstr)
{
    List* arrList = getArrList();
    int size = getSize();
    

    for (int i = 0; i < size; i++)
    {
        if (arrList[i].name == name)
        {
            if (field == 1)
            {
                arrList[i].name = newstr;
            }
            if (field == 2)
            {
                arrList[i].description = newstr;
            }
            if (field == 3)
            {
                arrList[i].data = newstr;
            }
            if (field == 4)
            {
                arrList[i].executionTime = newstr;
            }
            if (field == 5)
            {
                arrList[i].priority = newstr;
            }
        }
    }

    return arrList;

}

void editFile(string name, int field, string newstr)
{
    List* arrList = editList(name, field, newstr);
    int size = getSize();
    string str;
    ofstream file;
    file.open("toDo\\output.txt");

    if (file.is_open())
    {
        for (int i = 0; i < size; i++)
        {
            file << "#" << endl;
            file << "Name of case: " << arrList[i].name << endl;
            file << "Description of case: " << arrList[i].description << endl;
            file << "Data of case: " << arrList[i].data << endl;
            file << "Execution time of case: " << arrList[i].executionTime << endl;
            file << "Priority: " << arrList[i].priority << endl;
            file << endl;
        }
    }
    else
    {
        cout << "Erorr!";
    }
    file.close();
    delete[] arrList;
}

void makeChoiceEdit()
{
    List* arrList = getArrList();
    int size = getSize();
    string str;
    int field;
    string newstr;

    cout << "Enter the name of the case you want to edit\n" << endl;
    for (int i = 0; i < size; i++)
    {
        cout << endl;
        cout << "#" << endl;
        cout << "Name of case: " << arrList[i].name << endl;
        cout << "Description of case: " << arrList[i].description << endl;
        cout << "Data of case: " << arrList[i].data << endl;
        cout << "Execution time of case: " << arrList[i].executionTime << endl;
        cout << "Priority: " << arrList[i].priority << endl;
        cout << endl;
    }

    cout << endl;
    cout << "Enter name : ";
    cin.ignore();
    getline(cin, str);
    cout << endl;
    cout << "Enter field for editing:\n"
        << "1 - name\n"
        << "2 - description\n"
        << "3 - data\n"
        << "4 - execution time\n"
        << "5 - priority\n";
    cout << endl;
   
    cin >> field;
    cout << endl;
    if (field == 1)
    {
        cout << "Change name of the case : ";
        cin.ignore();
        getline(cin, newstr);
    }
    if (field == 2)
    {
        cout << "Change description of the case : ";
        cin.ignore();
        getline(cin, newstr);
    }
    if (field == 3)
    {
        cout << "Change data of the case : ";
        cin.ignore();
        getline(cin, newstr);
    }
    if (field == 4)
    {
        cout << "Change execution time of the case : ";
        cin.ignore();
        getline(cin, newstr);
    }
    if (field == 5)
    {
        cout << "Change priority of the case : ";
        cin.ignore();
        getline(cin, newstr);
    }
    delete[] arrList;
    editFile(str, field, newstr);
    

}

List* sortListName()
{
    List* arrList = getArrList();
    int size = getSize();
    List temp;

    for (int i = 0; i < size; i++)
    {
        for (int j = size - 1; j > i; j--)
        {

            if (arrList[j - 1].name > arrList[j].name)
            {
                temp = arrList[j - 1];
                arrList[j - 1] = arrList[j];
                arrList[j] = temp;
            }
        }
    }

    return arrList;

}

void showAfterSort()
{
    List* arrList = sortListName();
    int size = getSize();
    for (int i = 0; i < size; i++)
    {
        cout << "#" << endl;
        cout << "Name of case: " << arrList[i].name << endl;
        cout << "Description of case: " << arrList[i].description << endl;
        cout << "Data of case: " << arrList[i].data << endl;
        cout << "Execution time of case: " << arrList[i].executionTime << endl;
        cout << "Priority: " << arrList[i].priority << endl;
        cout << endl;
    }
    delete[] arrList;
}

void sortListPriority()
{
    List* arrList = getArrList();
    int size = getSize();
    for (int i = 0; i < size; i++)
    {
        if (arrList[i].priority == "High")
        {
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }
    for (int i = 0; i < size; i++)
    {
        if (arrList[i].priority == "Medium")
        {
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }
    for (int i = 0; i < size; i++)
    {
        if (arrList[i].priority == "Low")
        {
            cout << "#" << endl;
            cout << "Name of case: " << arrList[i].name << endl;
            cout << "Description of case: " << arrList[i].description << endl;
            cout << "Data of case: " << arrList[i].data << endl;
            cout << "Execution time of case: " << arrList[i].executionTime << endl;
            cout << "Priority: " << arrList[i].priority << endl;
            cout << endl;
        }

    }
    delete[] arrList;
}


void showMainMenu()
{
    cout << "\t\t\t---TO DO LIST---\n";
    cout << endl;

    int act = 0;

    while (act != 7)
    {
        cout << endl;
        cout << "\tEnter action : \n" << endl
            << "1 - Open to-do list\n"
            << "2 - Write a new case\n"
            << "3 - Edit a case\n"
            << "4 - Find a case\n"
            << "5 - Delete a case\n"
            << "6 - Sort cases\n"
            << "7 - Exite\n" << endl;

        cout << endl;
        cin >> act;
        cout << endl;

        Menu menu = (Menu)act;

        switch (menu)
        {
        case Menu::OPEN_CASE:
        {
            readList();
            break;
        }
        case Menu::WRITE_CASE:
        {
            int numberCase;
            cout << "How many cases you want to add?\n";
            cin >> numberCase;
            List* arrList = new List[numberCase];

            writeList(arrList, numberCase);
            delete[] arrList;
            break;
        }
        case Menu::EDIT_CASE:
        {
            makeChoiceEdit();
            cout << endl;
            cout << "The case was edit";
            break;
        }
        case Menu::FIND_CASE:
        {
            enum class Search
            {
                SEARCH_NAME = 1,
                SEARCH_PRIOPITY,
                SEARCH_DESCRIPTION,
                SEARCH_EXECUTION_TIME
            };

            int act;
            cout << "Search:\n" << endl
                << "1 - by name\n"
                << "2 - by priority\n"
                << "3 - by description\n"
                << "4 - by execution time" << endl;
            cout << endl;
            cin >> act;

            Search search = (Search)act;

            switch (search)
            {
            case Search::SEARCH_NAME:
            {
                findList();
                break;
            }
            case Search::SEARCH_PRIOPITY:
            {
                findList1();
                break;
            }
            case Search::SEARCH_DESCRIPTION:
            {
                findList2();
                break;
            }
            case Search::SEARCH_EXECUTION_TIME:
            {
                findList3();
                break;
            }

            }
            break;

        }
        case Menu::DELETE_CASE:
        {
            makeChoiceDelete();
            cout << endl;
            cout << "The case was delete\n";
            break;
        }
        case Menu::SORT_CASES:
        {
            int act;
            cout << "Enter sort cases :\n"
                << "1 - by name\n"
                << "2 - by priority\n";

            cin >> act;

            if (act == 1)
            {
                showAfterSort();
            }

            if (act == 2)
            {
                sortListPriority();
            }
            break;
        }
        }

    }

}

