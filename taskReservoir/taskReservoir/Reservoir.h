#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>

using namespace std;


class Reservoir
{
	char* name;
	char* type;
	uint32_t depth;
	uint32_t width;
	uint32_t length;
	

public:

	Reservoir(const char* nameP, const char* typeP, const uint32_t depthP, const uint32_t widthP,
		const uint32_t lengthP) :
		name{ nameP ? new char[strlen(nameP) + 1] : nullptr }, 
		type{ typeP ? new char[strlen(typeP) + 1] : nullptr },
		depth{ depthP }, width{ widthP }, length{ lengthP }
	{
		strcpy_s(name, strlen(nameP) + 1, nameP);
		strcpy_s(type, strlen(typeP) + 1, typeP);
		
	}

	Reservoir() : Reservoir { new char[100], new char[30], 1, 1, 1}{}

	Reservoir(const Reservoir& res) : name{new char[strlen(res.name) + 1]}, 
		type{new char[strlen(res.type) + 1]}, depth{res.depth}, 
		width{res.width}, length{res.length}
	{
		strcpy_s(name, strlen(res.name) + 1, res.name);
		strcpy_s(type, strlen(res.type) + 1, res.type);
	}
	 Reservoir(Reservoir&& res) : name{res.name}, type{res.type}, 
		 depth{ res.depth }, width{ res.width }, length{ res.length }
	 {
		 res.name = nullptr;
		 res.type = nullptr;
		 res.depth = 0;
		 res.width = 0;
		 res.length = 0;
	 }

	 Reservoir& operator=(const Reservoir& res)
	 {
		 if (this == &res)
		 {
			 return *this;
		 }

		 delete[] name;
		 delete[] type;

		 name = new char[strlen(res.name) + 1];
		 type = new char[strlen(res.type) + 1];
		 strcpy_s(name, strlen(res.name) + 1, res.name);
		 strcpy_s(type, strlen(res.type) + 1, res.type);

		 depth = res.depth;
		 width = res.width;
		 length = res.length;

		 return *this;
	 }

	friend ostream& operator<<(ostream& out, const Reservoir& res)
	{
		cout << endl;
		cout << "Name: ";
		out << res.name;
		cout << endl;
		cout << "Type: ";
		out << res.type;
		cout << endl;
		cout << "Depth in meters: ";
		out << res.depth;
		cout << endl;
		cout << "Width in meters: ";
		out << res.width;
		cout << endl;
		cout << "Length in meters: ";
		out << res.length;
		cout << endl;
		
		return out;
	}

	friend istream& operator>>(istream& in, Reservoir& res)
	{
		cout << endl;
		cout << " Enter name of reservoir: ";
		in >> res.name;
		cout << endl;
		cout << "Enter type (see, lake, pond): ";
		in >> res.type;
		cout << endl;
		cout << "Enter depth in meters: ";
		in >> res.depth;
		cout << endl;
		cout << "Enter width in meters: ";
		in >> res.width;
		cout << endl;
		cout << "Enter length in meters: ";
		in >> res.length;

		return in;

	}

	int getVolume() const
	{
		return depth * length * width;;
	}

	int getSquare() const
	{
		return length * width;
	}

	bool getCmpType(const Reservoir& res1, const Reservoir& res2)
	{
		if (strcmp(res1.type, res2.type))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	void getCmpSquareOneType(const Reservoir& res1, const Reservoir& res2)
	{
		if (getCmpType(res1, res2))
		{
			int square1 = res1.getSquare();
			int square2 = res2.getSquare();

			if (square1 > square2)
			{
				cout << endl << "The square " << res1.name << " bigger" << endl;
			}
			else if (square1 < square2)
			{
				cout << endl << "The square " << res2.name << " bigger" << endl;
			}
			else
			{
				cout << endl << "The areas are equal" << endl;
			}
		}
		else
		{
			cout << "Different type";
		}
		
	}


	


	void writeFile(const Reservoir* res, const int quantity) const
	{
		ofstream out;
		out.open("Reservoirs\\list.txt", ios::out | ios::app);
		if (out.is_open())
		{
			
			for (int i{ 0 }; i < quantity; i++)
			{
				out << "#" << endl;
				out <<"Name:"<< res[i].name << endl;
				out <<"Type:"<< res[i].type << endl;
				out <<"Depth in meters:" << res[i].depth << endl;
				out <<"Width in meters:" << res[i].width << endl;
				out <<"Length in meters:" << res[i].length << endl;
				out << endl;
			}
			out.close();
		}
		else
		{
			cout << "Erorr!";
		}

	}


	void readList()
	{
		string str;
		ifstream in;
		in.open("Reservoirs\\list.txt");
		if (in.is_open())
		{

			while (getline(in, str))
			{
				cout << str << endl;
			}
			in.close();
		}
		else
		{
			cout << "Erorr";
		}

	}

	Reservoir* addObj(Reservoir* res, int quantity, int add)
	{
		int size = quantity + add;
		Reservoir* result = new Reservoir[size];
		for (int i{ 0 }; i < size; i++)
		{
			if (i < quantity)
			{
				result[i] = res[i];
			}
			else
			{
				cin >> result[i];
				cout << endl << endl;
			}

		}

		delete[] res;

		return result;
	}

	Reservoir* deleteObj(Reservoir* res, int size, int idx)
	{
		Reservoir* result = new Reservoir[size - 1];
		int j{ 0 };
		for (int i{ 0 }; i < size; i++)
		{
			if (i != idx)
			{
				result[j] = res[i];
				j++;
			}
			else
			{
				continue;
			}
		}

		delete[] res;

		return result;

	}


	void showArrayRes( const Reservoir* res, const int quantity) const
	{
		for (int i{ 0 }; i < quantity; i++)
		{
			cout << res[i] << endl;
		}
	}

	Reservoir* fillArrayRes(int quantity)
	{
		Reservoir* res = new Reservoir[quantity];
		for (int i{ 0 }; i < quantity; i++)
		{
			cin >> res[i];
			cout << endl << endl;
		}

		return res;
	}

	~Reservoir()
	{
		if (name)
		{
			delete[] name;
		}

		if (type)
		{
			delete[] type;
		}
	}

};

