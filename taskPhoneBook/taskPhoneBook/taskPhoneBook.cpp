// taskPhoneBook.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "phoneBook.h"

enum class Menu
{
    OPEN_BOOK = 1,
    ADD_PERSON,
    FIND_PERSON,
    DELETE_PERSON
};


int main()
{
    phoneBook person;

    cout << "\t\t\t---PHONE BOOK---\n";
    cout << endl;

    int act = 0;

    while (act != 5)
    {
        cout << endl;
        cout << "\tEnter action : \n" << endl
            << "1 - Open phone book\n"
            << "2 - Add a new person\n"
            << "3 - Find a person\n"
            << "4 - Delete a person\n"
            << "5 - Exite\n" << endl;

        cout << endl;
        cin >> act;
        cout << endl;

        Menu menu = (Menu)act;

        switch (menu)
        {
        case Menu::OPEN_BOOK:
        {
            person.readFile();
            break;
        }
        case Menu::ADD_PERSON:
        {
            int numberPersons;
            cout << "How many persons you want to add?\n";
            cin >> numberPersons;
            phoneBook* persons = new phoneBook[numberPersons];

            persons->addInTheFile(persons, numberPersons);
            delete[] persons;
            break;
        }
        case Menu::FIND_PERSON:
        {
            person.findPerson();
            break;

        }
        case Menu::DELETE_PERSON:
        {
            person.makeChoiceDelete();
            cout << endl;
            cout << "The person was delete\n";
            break;
        }

        }

    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
