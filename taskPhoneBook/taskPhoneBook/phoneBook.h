#pragma once
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class phoneBook
{
	string name;
	string workPhone;
	string mobile;
	string homePhone;
	string info;


    phoneBook* getPersons()
    {

        int size = 0;
        int sizebuf = 0;

        string* ptrList = nullptr;
        string* bufList = nullptr;

        ifstream file;
        file.open("phoneBook\\book.txt");
        if (file.is_open())
        {
            string oneList;

            while (!file.eof())
            {
                getline(file, oneList);
                if (oneList == "#")
                {
                    size++;
                }
                else if (oneList.empty())
                {
                    continue;
                }
                else
                {
                    int len = oneList.length();
                    int count = 0;
                    for (int i = 0; i < len; i++)
                    {
                        if (oneList[i] == ':')
                        {
                            count += 2;
                            break;
                        }
                        count++;
                    }
                    oneList.erase(0, count);

                    sizebuf++;
                    bufList = new string[sizebuf];

                    for (int i = 0; i < sizebuf - 1; i++)
                    {
                        bufList[i] = ptrList[i];
                    }
                    bufList[sizebuf - 1] = oneList;

                    delete[] ptrList;

                    ptrList = bufList;
                    bufList = nullptr;
                }


            }

            phoneBook* persons = new phoneBook[size];
            int p = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < 1; j++)
                {
                    persons[i].name = ptrList[j + p];
                    p++;
                    persons[i].workPhone = ptrList[j + p];
                    p++;
                    persons[i].mobile = ptrList[j + p];
                    p++;
                    persons[i].homePhone = ptrList[j + p];
                    p++;
                    persons[i].info = ptrList[j + p];
                    p++;
                }
            }


            delete[] ptrList;

            return persons;
        }
        else
        {
            cout << "Erorr!";
        }
        file.close();
    }

    int getSize()
    {
        int size = 0;
        ifstream file;
        file.open("phoneBook\\book.txt");
        if (file.is_open())
        {
            string oneList;

            while (!file.eof())
            {
                getline(file, oneList);
                if (oneList == "#")
                {
                    size++;
                }
            }

            return size;
        }
        else
        {
            cout << "Erorr!";
        }
        file.close();

    }


public:

	phoneBook(string nameP, string workPhoneP,
		string mobileP, string homePhoneP, string infoP) :
		name{nameP},
		workPhone{ workPhoneP},
		mobile{ mobileP },
		homePhone{ homePhoneP},
		info{ infoP }
	{

	}

	phoneBook() : phoneBook {" ", " ", " ", " ", " "} {}
	
	phoneBook(const phoneBook& person) = delete;


	friend ostream& operator<<(ostream& out, const phoneBook& person)
	{
		cout << endl;
		out << "name: " << person.name << endl;
		out << "work phone: " << person.workPhone << endl;
		out << "mobile: " << person.mobile << endl;
		out << "home phone: " << person.homePhone << endl;
		out << "additional information: " << person.info << endl;

		return out;
	}

	friend istream& operator>>(istream& in, phoneBook& person)
	{
		cout << endl;
		cout << "Enter name: ";
        in.ignore();
        getline(in, person.name);
		cout << "Enter work phone: ";
		getline(in, person.workPhone);
		cout << "Enter mobile: ";
		getline(in, person.mobile);
		cout << "Enter home phone: ";
		getline(in, person.homePhone);
		cout << "Enter additional information: ";
		getline(in, person.info);

		return in;
	}

    void addInTheFile(phoneBook* persons, const int numberPerson)
    {
        for (int i = 0; i < numberPerson; i++)
        {
            cout << "-----------------------\n";
            cin >> persons[i];
            cout << "-----------------------\n";
        }

        ofstream out;
        out.open("phoneBook\\book.txt", ios::out | ios::app);

        if (out.is_open())
        {
            for (int i = 0; i < numberPerson; i++)
            {
                out << "#" << endl;
                out << persons[i] << endl;
                out << endl;
            }
            out.close();
        }
        else
        {
            cout << "Erorr!";
        }

    }
        void readFile()
        {
            string str;
            ifstream in;
            in.open("phoneBook\\book.txt");
            if (in.is_open())
            {

                while (getline(in, str))
                {
                    cout << str << endl;
                }
                in.close();
            }
            else
            {
                cout << "Erorr";
            }

        }

        void findPerson()
        {
            phoneBook* persons = getPersons();
            int size = getSize();
            string str;

            cout << "Enter name of the person : \n";
            cin.ignore();
            getline(cin, str);

            for (int i = 0; i < size; i++)
            {
                if (persons[i].name == str)
                {
                    cout << persons[i];
                }

            }

            delete[] persons;
        }

        void deletePerson(const string name)
        {
            phoneBook* persons = getPersons();
            int size = getSize();

            ofstream file;
            file.open("phoneBook\\book.txt");

            if (file.is_open())
            {
                for (int i = 0; i < size; i++)
                {
                    if (persons[i].name == name)
                    {
                        continue;
                    }
                    file << "#" << endl;
                    file << persons[i];
                    file << endl;
                }
            }
            else
            {
                cout << "Erorr!";
            }
            file.close();
            delete[] persons;
        }

        void makeChoiceDelete()
        {
            phoneBook* persons = getPersons();
            int size = getSize();
            string name;

            cout << "Enter the name of the person you want to delete\n" << endl;
            readFile();

            cout << endl;
            cout << "Enter name : ";
            cin.ignore();
            getline(cin, name);
            delete[] persons;
            deletePerson(name);

        }



	~phoneBook()
	{
        
	}

   


	
};
