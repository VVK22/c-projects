#include "Fraction.h"

int Fraction::findCommonDenom(int den1, int den2)
{
	int common = 1;
	if (den1 % den2 == 0)
	{
		common = den1;
	}
	else if (den2 % den1 == 0)
	{
		common = den2;
	}
	else
	{
		common = den1 * den2;
	}

	return common;
}

void Fraction::show()
{
	if (numerator < denom)
	{
		cout <<"fraction = " << numerator << "\\" << denom << endl;
	}
	else if(numerator > denom)
	{
		double a = (double)numerator /(double)denom;
		cout << "fraction = " << numerator << "\\" << denom << " = " << a << endl;
	}
	else
	{
		cout <<"fraction = " << numerator << "\\" << denom << " = " << " 1\n";
	}
}

void Fraction::plus(Fraction& fraction1, Fraction& fraction2)
{
	
	int common = findCommonDenom(fraction1.getDenom(), fraction2.getDenom());
	int firstNum = common / fraction1.getDenom() * fraction1.getNumerator();
	int secondNum = common / fraction2.getDenom() * fraction2.getNumerator();
	numerator = firstNum + secondNum;
	denom = common;
	show();
}

void Fraction::minus(Fraction& fraction1, Fraction& fraction2)
{
	
	int common = findCommonDenom(fraction1.getDenom(), fraction2.getDenom());
	int firstNum = common / fraction1.getDenom() * fraction1.getNumerator();
	int secondNum = common / fraction2.getDenom() * fraction2.getNumerator();
	numerator = firstNum - secondNum;
	denom = common;
	show();
}

void Fraction::multiply(Fraction& fraction1, Fraction& fraction2)
{
	
	numerator = fraction1.getNumerator() * fraction2.getNumerator();
	denom = fraction1.getDenom() * fraction2.getDenom();
	show();
}

void Fraction::divide(Fraction& fraction1, Fraction& fraction2)
{
	
	numerator = fraction1.getNumerator() * fraction2.getDenom();
	denom = fraction2.getNumerator() * fraction1.getDenom();
	show();
}

