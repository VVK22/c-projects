#pragma once
#include <iostream>

using namespace std;

class Fraction
{
	int numerator;
	int denom;

	int findCommonDenom(int den1, int den2);
    
    void show();

public:

	Fraction() : numerator{ 1 }, denom{ 1 } {}

    int inline getNumerator()
    {
        return numerator;
    }
    void inline setNumerator(int num)
    {
       
        numerator = num;
    }

    int inline getDenom()
    {
        return denom;
    }
    void inline setDenom(int den)
    {
        if (den == 0)
        {
            denom = 1;

        }
        else
        denom = den;
    }

    

    void plus(Fraction& fraction1, Fraction& fraction2);
    void minus(Fraction& fraction1, Fraction& fraction2);
    void multiply(Fraction& fraction1, Fraction& fraction2);
    void divide(Fraction& fraction1, Fraction& fraction2);

};

