// taskFraction.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Fraction.h"

void showMenu()
{
    int n1, n2;
    int d1, d2;
    int act = 0;

    Fraction a;
    Fraction f1;
    Fraction f2;

    cout << "Enter the first fraction :\n";
    cout << "numerator : ";
    cin >> n1;
    cout << endl;
    cout << "denomerator : ";
    cin >> d1;
    cout << endl;

    cout << "Enter the second fraction :\n";
    cout << "numerator : ";
    cin >> n2;
    cout << endl;
    cout << "denomerator : ";
    cin >> d2;
    cout << endl;

    f1.setNumerator(n1);
    f1.setDenom(d1);
    f2.setNumerator(n2);
    f2.setDenom(d2);
    while (act != 5)
    {
        cout << "Enter an action : \n"
            << "1 - add fractions\n"
            << "2 - subtract fractions\n"
            << "3 - multiply fractions\n"
            << "4 - divide fractions\n"
            << "5 - exite\n";
        cin >> act;
        switch (act)
        {
        case 1:
        {
            a.plus(f1, f2);
            break;
        }
        case 2:
        {
            a.minus(f1, f2);
            break;
        }
        case 3:
        {
            a.multiply(f1, f2);
            break;
        }
        case 4:
        {
            a.divide(f1, f2);
            break;
        }
        }
        cout << endl;
    }
    
    
}

int main()
{
    showMenu();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
