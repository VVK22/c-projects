#pragma once

#include <iostream>
using namespace std;

class Stack
{
	enum{empty = -1, full = 30};
	char stack[full + 1];
	int top;
public:
	
	Stack() {
		top = empty;
	}

	Stack(const Stack& stack) = delete;

	void clear()
	{
		top = empty;
	}

	bool isEmpty()
	{
		return top == empty;
	}

	bool isFull()
	{
		return top == full;
	}

	void push(int element)
	{
		if (!isFull())
		{
			top++;
			stack[top] = element;
		}
	}

	int pop()
	{
		if (!isEmpty())
		{
			return stack[top--];
		}
		else
		{
			return -1;
		}
	}

	int peek()
	{
		if (!isEmpty())
		{
			return stack[top];
		}
		else
		{
			return -1;
		}
	}

	int getCount()
	{
		return top + 1;
	}
	
	

};

