#pragma once
#include <iostream>

using namespace std;

struct Node
{
	int value;
	Node* left;
	Node* right;
};

class Tree
{
	Node* root;

	void destroyTree(Node* leaf)
	{
		if (leaf != nullptr)
		{
			destroyTree(leaf->left);
			destroyTree(leaf->right);
			delete leaf;
		}
	}

	void insert(int key, Node* leaf)
	{
		if (key < leaf->value)
		{
			if (leaf->left != nullptr)
			{
				insert(key, leaf->left);
			}
			else
			{
				leaf->left = new Node;
				leaf->left->value = key;
				leaf->left->left = nullptr;
				leaf->left->right = nullptr;
			}
		}
		else if(key >= leaf->value)
		{
			if (leaf->right != nullptr)
			{
				insert(key, leaf->right);
			}
			else
			{
				leaf->right = new Node;
				leaf->right->value = key;
				leaf->right->right = nullptr;
				leaf->right->left = nullptr;
			}
			
		}
		
	}

	Node* search(int key, Node* leaf)
	{
		if (leaf != nullptr)
		{
			if (key == leaf->value)
			{
				return leaf;
			}
			if (key < leaf->value)
			{
				return search(key, leaf->left);
			}
			else
			{
				return search(key, leaf->right);
			}

		}
		else
		{
			return nullptr;
		}
	}

	void inorderPrint(Node* leaf)
	{
		if (leaf != nullptr)
		{
			inorderPrint(leaf->left);
			cout << leaf->value << " ";
			inorderPrint(leaf->right);
		}
	}

	void postorderPrint(Node* leaf)
	{
		if (leaf != nullptr)
		{
			inorderPrint(leaf->left);
			inorderPrint(leaf->right);
			cout << leaf->value << " ";
		}
	}

	void preorderPrint(Node* leaf)
	{
		if (leaf != nullptr)
		{
			cout << leaf->value << " ";
			inorderPrint(leaf->left);
			inorderPrint(leaf->right);
		}
	}
	
public:
	
	Tree() : root{ nullptr } {}

	~Tree()
	{ 
		destroyTree();
	}

	void destroyTree()
	{
		destroyTree(root);
	}

	void insert(int key)
	{
		
		if (root != nullptr)
		{
			insert(key, root);
		}
		else
		{
			root = new Node;
			root->value = key;
			root->left = nullptr;
			root->right = nullptr;
		}
		
	}

	Node* search(int key)
	{
		return search(key, root);
	}

	void inorderPrint()
	{
		inorderPrint(root);
		cout << endl;
	}

	void postorderPrint()
	{
		postorderPrint(root);
		cout << endl;
	}

	void preorderPrint()
	{
		preorderPrint(root);
		cout << endl;
	}
};

